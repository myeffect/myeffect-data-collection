## The print statement prints the wrong number of full_url_list(a lot more than it should be)
## Possibly because I have placed it in the wrong place.. but it is checked and stores all the jobs including hidden jobs in the csv


import requests
import pandas as pd
import time
import numpy as np

from selenium import webdriver
import warnings

# driver for selenium (the web browser)
from selenium.webdriver.common.by import By

warnings.filterwarnings("ignore", category=DeprecationWarning)

driver = webdriver.Chrome("C:\R6\chromedriver.exe")

# Part 1 Get Links

# First page of location on volunteer match
location_name = 'VolunteerMatch'

# This link contains volunteer jobs from Toronto, ON
# update the link if you want to grab volunteer info from other provinces
link = 'https://www.volunteermatch.org/search/?l=Toronto%2C+ON%2C+Canada&v=true'
# link = 'https://www.volunteermatch.org/search/?l=%E5%8A%A0%E6%8B%BF%E5%A4%A7%E5%AE%89%E5%A4%A7%E7%95%A5%E7%9C%81%E5%A4%9A%E4%BC%A6%E5%A4%9A'
# this is newest link = 'https://www.volunteermatch.org/search/?l=Toronto%2C+ON%2C+Canada&v=true&cats=13%2C7%2C23%2C38%2C37%2C15%2C29%2C40%2C33%2C43%2C3%2C12%2C17%2C5%2C39%2C36%2C28%2C14%2C30%2C22%2C34%2C25%2C42%2C27%2C11%2C31%2C41%2C19%2C6'
driver.get(link)


def get_url(url, hidden=False):
    url_list = []
    driver.get(url)
    time.sleep(4)
    if hidden == False:
        x = driver.find_elements_by_xpath("//*[@class='pub-srp-opps__title ga-track-to-opp-details']")
        for a in x:
            url_list.append(a.get_attribute('href'))
    else:
        x = driver.find_elements_by_xpath("//*[@class='link-body-text pub-srp-opps__title ga-track-to-opp-details']")
        for a in x:
            url_list.append(a.get_attribute('href'))
    return url_list

# step 1
# Scrape all website links from shown volunteer jobs on the current page
full_url_list = get_url(link)

#print(full_url_list)

# step 2
# Scraping all website links from NOT shown (some jobs have "# More Active Opportunities") volunteer jobs on the current page
# Button unclickable error occurs sometimes, I guess it's about the internet issue. Wait for longer time or refresh the page can solve this problem

def get_hidden_jobs(full_url_list):
    #print("ok")
    #btn = driver.find_elements_by_xpath('//*[@class = "pub-srp-opps__org-link org-link"]')
    btn = driver.find_elements(By.XPATH, '//*[@class = "pub-srp-opps__org-link org-link"]')
    for i in range(len(btn)):
        #btn = driver.find_elements_by_xpath('//*[@class = "pub-srp-opps__org-link org-link"]')
        btn = driver.find_elements(By.XPATH, '//*[@class = "pub-srp-opps__org-link org-link"]')
        time.sleep(4)
        btn[i].click()
        time.sleep(4)
        curr_url = driver.current_url
        url_list = get_url(curr_url, hidden=True)
        full_url_list.extend(url_list)

        while True:
            try:
                #c = driver.find_element_by_xpath('//*[@id="pub_org_srp_app"]')
                #driver.find_element_by_xpath('//*[@class = "pub-srp-pag__arrw pub-srp-pag__arrw--next"]').click()
                driver.find_element_by_xpath('//*[@class = "pub-srp pub-srp--org__back hvr-o"]').click()


                # curr_url = driver.current_url
                # url_list = get_url(curr_url, hidden = True) # ,hidden =True
                # full_url_list.extend(url_list)

            except:
                break
        driver.get(link)
        driver.refresh()
        time.sleep(4)
    return list(set(full_url_list))



full_url_list = get_hidden_jobs(full_url_list)

#print(len(full_url_list))

#print(full_url_list)
# step 2
# Go to the next page and grab all links from shown volunteer jobs until finished if get_hidden_jobs = True, otherwise ignore the hidden volunteer jobs.

def get_all_pages(full_url_list, location_name):
    #while True: # This will go for all the website pages
    for i in range(2):
        try:
            # if get_hidden_jobs == True:
            #     full_url_list = get_hidden_jobs(full_url_list)
            # driver.refresh()
            # time.sleep(5)
            driver.find_element_by_xpath("//*[contains(text(), 'Next')]").click()
            time.sleep(4)
            print("Next page")
            # btn = driver.find_elements(By.XPATH, '//*[@class = "pub-srp-opps__org-link org-link"]')
            # for i in range(len(btn)):
            #     # btn = driver.find_elements_by_xpath('//*[@class = "pub-srp-opps__org-link org-link"]')
            #     btn = driver.find_elements(By.XPATH, '//*[@class = "pub-srp-opps__org-link org-link"]')
            #     time.sleep(4)
            #     btn[i].click()
            #     time.sleep(4)
            #
            #     link = driver.current_url
            #     url_list = get_url(link)
            #     full_url_list.extend(url_list)

            print('Total No. of URLs now: {}'.format(len(full_url_list)))

            btn = driver.find_elements(By.XPATH, '//*[@class = "pub-srp-opps__org-link org-link"]')
            #print(len(btn))
            for i in range(len(btn)):
                btn = driver.find_elements(By.XPATH, '//*[@class = "pub-srp-opps__org-link org-link"]')
                #print(len(btn))
                time.sleep(4)
                btn[i].click()
                time.sleep(4)
                curr_url = driver.current_url
                url_list = get_url(curr_url, hidden=True)  # ,hidden =True
                full_url_list.extend(url_list)
                # link = driver.current_url
                # url_list = get_url(link)
                # full_url_list.extend(url_list)

                #############################################
                # For companies with more than 25 jobs
                while True:
                    try:
                        driver.find_element_by_xpath('//[@class = "pub-srp-pag__arrw pub-srp-pag__arrw--next"]').click()
                        curr_url = driver.current_url
                        url_list = get_url(curr_url, hidden=True)
                        full_url_list.extend(url_list)

                    except:
                        break

                ###############################################
                while True:
                    try:
                        driver.find_element_by_xpath('//*[@class = "pub-srp pub-srp--org__back hvr-o"]').click()

                        # curr_url = driver.current_url
                        # url_list = get_url(curr_url, hidden=True)  # ,hidden =True
                        # full_url_list.extend(url_list)

                        link = driver.current_url
                        url_list = get_url(link)
                        full_url_list.extend(url_list)
                        #print(len(full_url_list))
                    except:
                        break
                    driver.get(link)
                    driver.refresh()
                    time.sleep(4)


        except:
            print("Finished, {} URLs in total".format(len(full_url_list)))
            break

    #return list(set(full_url_list))  # This or the one under, need to find out.
    return full_url_list

#print(full_url_list)
full_url_list = get_all_pages(full_url_list, location_name)

print(len(full_url_list))
#print(full_url_list)

# Save links to a csv file.
df = pd.DataFrame({'Link': full_url_list})
df.to_csv('{}.csv'.format(location_name), index=False, encoding='utf-8_sig')  # link storing / prev location_name

## Part 2 Get Info
# Get volunteer job information, including job title, job description, job categories

df = pd.read_csv('{}.csv'.format(location_name), encoding='utf-8')
full_url_list = df['Link'].tolist()

# The following info will be scraped:

title = []
description = []
organization = []
num_interest = []
organization_description = []
when = []
where = []
date_posted = []
categories = []
requirements = []
valid_urls = []

for i in range(len(full_url_list)):
    if full_url_list[i] in valid_urls:
        continue

    driver.get(full_url_list[i])

    # Get job title
    # x = driver.find_elements_by_xpath('//*[contains(@class, "opp-dtl__title--main")]')
    x = driver.find_elements(By.XPATH, '//*[contains(@class, "opp-dtl__title--main")]')
    for a in x:
        title.append(a.text)

    # Get organization
    # x = driver.find_elements_by_xpath('//*[contains(@class, "link-body-text text-sm caps")]')
    x = driver.find_elements(By.XPATH, '//*[contains(@class, "link-body-text text-sm caps")]')
    for a in x:
        organization.append(a.text)

    # Get num of people interested
    x = driver.find_elements(By.XPATH, '//*[@id = "all_interested_sum"]')

    if not x:
        num_interest.append(np.nan)
    else:
        num_interest.append(x[0].text)

    # Get job description
    job_description_text = ''
    while True:
        try:
            # driver.find_elements_by_xpath('//*[contains(@class, "condense_control condense_control_more")]')[0].click()
            driver.find_elements(By.XPATH, '//*[contains(@class, "condense_control condense_control_more")]')[0].click()
            time.sleep(1)

            # x = driver.find_elements_by_xpath('//*[contains(@class, "opp-dtl__summary jq-modifier")]')
            x = driver.find_elements(By.XPATH, '//*[contains(@class, "opp-dtl__summary jq-modifier")]')
            for a in x:
                if a.text[-9:].lower() == 'show less':
                    job_description_text += a.text[:-9]
                else:
                    job_description_text += a.text

            break
        except:
            # x = driver.find_elements_by_xpath('//*[contains(@class, "opp-dtl__summary")]')
            x = driver.find_elements(By.XPATH, '//*[contains(@class, "opp-dtl__summary")]')
            for a in x:
                job_description_text += a.text

            break

    description.append(job_description_text)

    # Get organization description
    # x = driver.find_elements_by_xpath('//*[@id="tertiary-content"]')
    x = driver.find_elements(By.XPATH, '//*[@id="tertiary-content"]')
    for a in x:
        content = a.text
        idx = content.index('Description')
        organization_description.append(content[idx + len('Description'):])

    # Get when, where and date posted
    # x = driver.find_elements_by_xpath('//*[contains(@class, "logistics__section logistics__section--when")]')
    x = driver.find_elements(By.XPATH, '//*[contains(@class, "logistics__section logistics__section--when")]')
    if x:
        when.append(x[0].text.split('\n')[1:])
    else:
        when.append(np.nan)

    # x = driver.find_elements_by_xpath('//*[contains(@class, "col-7 address")]')
    x = driver.find_elements(By.XPATH, '//*[contains(@class, "col-7 address")]')
    if x:
        t = x[0].text.split('\n')[1:]
        where.append(' '.join(t))
    else:
        where.append(np.nan)

    # x = driver.find_elements_by_xpath('//*[contains(@class, "logistics__section logistics__section--date-posted")]')
    x = driver.find_elements(By.XPATH, '//*[contains(@class, "logistics__section logistics__section--date-posted")]')
    if x:
        date_posted.append(x[0].text.split('\n')[1:])
    else:
        date_posted.append(np.nan)

    # Get categories
    # x = driver.find_elements_by_xpath('//*[contains(@class, "col-7 logistics__section logistics__section--skills")]')
    x = driver.find_elements(By.XPATH, '//*[contains(@class, "col-7 logistics__section logistics__section--skills")]')
    cat = []
    if x:
        for a in x:
            cat.append(a.text)
        categories.append(cat[0].split("\n")[1:])
    else:
        categories.append(np.nan)

    # Get requirements
    # x = driver.find_elements_by_xpath('//*[contains(@class, "logistics__section logistics__section--requirements")]')
    x = driver.find_elements(By.XPATH, '//*[contains(@class, "logistics__section logistics__section--requirements")]')
    if x:
        requirements.append(x[0].text.split("\n")[1:])
    else:
        requirements.append(np.nan)

    valid_urls.append(driver.current_url)

    time.sleep(1)

# save to csv file
df = pd.DataFrame({'Link': valid_urls, 'Title': title,
                   'Description': description, 'Categories': categories,
                   'Organization': organization, 'Numberofpeopleinterested': num_interest,
                   'When': when, 'Where': where, 'Dateposted': date_posted, 'Requirements': requirements,
                   'Organizationdescription': organization_description})
df.to_csv('{}.csv'.format(location_name), index=False, encoding='utf-8_sig')
df